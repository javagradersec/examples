import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class URLWebAccess {
	public static void main(String[] args) throws MalformedURLException, IOException {
		System.out.println(new URL("http://www.internet.org/").openConnection().getContentType());
	}
}