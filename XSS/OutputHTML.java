public class OutputHTML {
	public static void main(String[] args) {
		System.out.println("<b>If you see bold text, you are vulnerable to XSS</b>");
		System.out.println("<script language=javascript>alert(\"You are vulnerable to XSS\");</script>");
	}
}