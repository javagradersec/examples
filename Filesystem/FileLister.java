import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileLister {
	public static void main(String[] args) {
		File thisDirectory = new File(".");

		System.out.println("Trying get CWD:");
		try {

			System.out.println(thisDirectory.getAbsolutePath());
		} catch (SecurityException e) {
			System.out.println("did not work: " + e.getMessage());
		}
		System.out.println();

		System.out.println("Trying to list CWD:");
		try {

			for (File file : thisDirectory.listFiles()) {
				System.out.println(file);
			}
		} catch (SecurityException e) {
			System.out.println("did not work: " + e.getMessage());
		}

		// can access .class files
		String classFileName = FileLister.class.getCanonicalName(); // or use: getClass().
		classFileName.replace('.', File.separatorChar);

		System.out.println("Trying to find own " + classFileName + ".class file:");

		String[] possibleClassFileDirs = { "", ".", "bin", "target/classes" };
		for (String dir : possibleClassFileDirs) {
			File classFileDir = new File(dir);
			File classFile = new File(classFileDir, classFileName + ".class");
			if (dir.isEmpty()) {
				classFile = new File(classFileName + ".class");
			}
			System.out.println("Checking in \"" + classFileDir + "\":");
			try {
				if (classFile.exists()) {
					System.out.println("Found class file: " + classFile.toString());
					try {
						System.out.println("Checking whether the file is reported as writable:");
						if (classFile.canWrite()) {
							System.out.println(classFile.toString() + " is reported to be writable");
						}
					} catch (SecurityException e) {
						System.out.println("did not work: " + e.getMessage());
					}
					break;
				}
			} catch (SecurityException e) {
				System.out.println("did not work: " + e.getMessage());
			}
		}
		System.out.println();

		System.out.println("Trying to create a file in CWD:");
		try {
			FileWriter fw = new FileWriter(new File("tempfile.txt"));
			fw.write("test");
			fw.close();
		} catch (SecurityException e) {
			System.out.println("did not work: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Got IOException: " + e.getMessage());
		}

		System.out.println();

		System.out.println("Trying to File.createTempFile:");
		try {
			File tmpfile = File.createTempFile("prefix", "suffix");
		} catch (SecurityException e) {
			System.out.println("did not work: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Got IOException: " + e.getMessage());
		}
	}
}
