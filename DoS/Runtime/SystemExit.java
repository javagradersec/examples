public class SystemExit {
	public static void main(String[] args) {
		System.out.println("Trying System.exit(0):");
		try {
			System.exit(0);
		} catch (SecurityException e) {
			System.out.println("did not work:" + e.getMessage());
		}
		System.out.println();
		
		System.out.println("Trying again w/o try-catch:");
		System.exit(0);
	}
}
