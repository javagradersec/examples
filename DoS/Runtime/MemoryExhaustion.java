

import java.util.ArrayList;
import java.util.List;

public class MemoryExhaustion {

	public static void main(String[] args) {
		List<byte[]> list = new ArrayList<byte[]>();
		int i = 0;
		while (true) {
			++i;
			byte[] array = new byte[1024*1024];
			list.add(array);
			System.out.println(i);
		}
	}
}
