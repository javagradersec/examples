public class EndlessLoopTimer {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		while (true) {
			System.out.println((System.currentTimeMillis() - start) / 1000);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("Ignore InterruptedException");
			}
		}
	}
}
