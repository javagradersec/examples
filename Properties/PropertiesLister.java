import java.security.AccessControlException;
import java.util.Enumeration;
import java.util.Properties;

public class PropertiesLister {
	public static void main(String[] args) {
		System.out.println("Trying to list all properties:");
		try {
			Properties p = System.getProperties();
			Enumeration keys = p.keys();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				String value = (String) p.get(key);
				System.out.println(key + ": " + value);
			}
		} catch (AccessControlException e) {
			System.out.println("Could not list all properties: " + e.getMessage());
		}
		System.out.println();
		System.out.println("Trying to list known properties:");

		for (String prop : new String[] { "java.runtime.name", "sun.boot.library.path", "java.vm.version", "java.vm.vendor", "java.vendor.url", "path.separator", "java.vm.name", "file.encoding.pkg", "user.country", "user.script", "sun.java.launcher", "sun.os.patch.level", "java.vm.specification.name", "user.dir", "java.runtime.version", "java.awt.graphicsenv", "java.endorsed.dirs", "os.arch", "java.io.tmpdir", "line.separator", "java.vm.specification.vendor", "user.variant", "os.name", "sun.jnu.encoding", "java.library.path", "java.specification.name", "java.class.version", "sun.management.compiler", "os.version", "user.home", "user.timezone", "java.awt.printerjob", "file.encoding", "java.specification.version", "java.class.path", "user.name", "java.vm.specification.version", "sun.java.command", "java.home", "sun.arch.data.model", "user.language", "java.specification.vendor", "awt.toolkit", "java.vm.info", "java.version", "java.ext.dirs", "sun.boot.class.path", "java.vendor", "sun.stderr.encoding", "file.separator", "java.vendor.url.bug", "sun.io.unicode.encoding", "sun.cpu.endian", "sun.stdout.encoding", "sun.desktop", "sun.cpu.isalist" }) {
			try {
				System.out.println(prop + ": " + System.getProperty(prop));
			} catch (AccessControlException e) {
				// ignore
			}
			System.out.println("finished.");
		}
	}
}
