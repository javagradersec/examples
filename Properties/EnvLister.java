import java.security.AccessControlException;
import java.util.Iterator;
import java.util.Map;

public class EnvLister {
	public static void main(String[] args) {
		System.out.println("Trying to list all environent variables:");
		try {
			Map<String, String> env = System.getenv();
			Iterator<String> it = env.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				String value = env.get(key);
				System.out.println(key + ": " + value);
			}
		} catch (AccessControlException e) {
			System.out.println("Could not list all environent variables: " + e.getMessage());
		}
		System.out.println();
		System.out.println("Trying to list known environent variables:");

		for (String prop : new String[] { "TMP", "TEMP", "WINDIR", "SHELL", "UID", "SSH_CLIENT", "LOGNAME", "USER", "windir", "OS", "HOME", "HOMEDRIVE", "HOMEPATH", "Path", "PATH"}) {
			try {
				System.out.println(prop + ": " + System.getenv(prop));
			} catch (AccessControlException e) {
				// ignore

			}
		}
		System.out.println("finished.");
	}
}
