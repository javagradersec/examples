public class Reflection {
	public static void main(String[] args) {
		System.out.println("Enumerating methods of java.util.ArrayList:");
		Class arrayList = null;
		try {
			arrayList = Class.forName("java.util.ArrayList");
			for (java.lang.reflect.Method method : arrayList.getMethods()) {
				System.out.println(method.toString());
			}
			System.out.println("----------------");
			System.out.println("Trying to enumerate all declared methods of java.util.ArrayList (requires RuntimePermission \"accessDeclaredMembers\" permission):");
			for (java.lang.reflect.Method method : arrayList.getDeclaredMethods()) {
				System.out.println(method.toString());
			}
			System.out.println("RuntimePermission \"accessDeclaredMembers\" seems to be granted");
		} catch (ClassNotFoundException e) {
			System.out.println("Could not load java.util.ArrayList");
			//e.printStackTrace();
		} catch (java.security.AccessControlException e) {
			System.out.println("Enumerating threw AccessControlException, supposedly RuntimePermission \"accessDeclaredMembers\" not granted");
			//e.printStackTrace();
			arrayList = null;
		}
		System.out.println("----------------");

		if (arrayList == null) {
			System.out.println("Skipping test to read private field (requires the RuntimePermission \"accessDeclaredMembers\" as precondition)");
			return;
		}
		System.out.println("Trying to read private field DEFAULT_CAPACITY (requires the ReflectPermission \"suppressAccessChecks\"):");
		try {
			java.lang.reflect.Field field = arrayList.getDeclaredField("DEFAULT_CAPACITY");
			field.setAccessible(true);
			System.out.println("ReflectPermission \"suppressAccessChecks\" seems to be granted");
		} catch (NoSuchFieldException e) {
			System.out.println("ArrayList seems to have changed, this test need to be updated!");
			//e.printStackTrace();
		} catch (SecurityException e) {
			System.out.println("ReflectPermission \"suppressAccessChecks\" seems not to be granted");
			//e.printStackTrace();
		}
	}
}
